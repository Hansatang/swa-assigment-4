import type { User } from '../domain/user';
import { useAuthStore } from '../store/authStore';
import env from '../environment';
import { okOrReject } from '@/utils/utils';
import { watch } from 'vue';
import { storeToRefs } from 'pinia';
import router from '../router';

export type LoginData = {
  username: string,
  password: string,
};

export type AuthResponse = {
  userId: string,
  token: string,
};

export function useRegisterUser() {
  return (user: User) => fetch(`${env.apiBaseUrl}/users`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', },
    body: JSON.stringify(user),
  }).then(okOrReject);
}

export function useLogin() {
  const store = useAuthStore();
  const authedFetch = useAuthenticatedFetch();

  return (dto: LoginData) => fetch(`${env.apiBaseUrl}/login`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', },
    body: JSON.stringify(dto),
  })
      .then(okOrReject)
      .then(r => r.json())
      .then((auth: AuthResponse) => authedFetch(`${env.apiBaseUrl}/users/${auth.userId}`, {}, auth.token)
            .then(okOrReject)
            .then(r => r.json())
            .then((user: User) => store.login(user, auth.token)));
}

export function useUpdateUser() {
  const authedFetch = useAuthenticatedFetch();
  const store = useAuthStore();

  return (updated: User) => {
    return authedFetch(`${env.apiBaseUrl}/users/${store.currentUser?.id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(updated),
    })
    .then(okOrReject)
    .then(() => store.updateProfile(updated));
  }
}

export function useAuthenticatedFetch() {
  const store = useAuthStore();
  return (url: string, init?: RequestInit, token?: string) => {
    const t = token ?? store.currentToken;

    if(!t)
      return Promise.reject('Not logged in');

    const sep = url.includes('?') ? '&' : '?';
    return fetch(`${url}${sep}token=${t}`, init);
  };
};

export function useRequireAuth() {
  const store = useAuthStore();

  const { isLoggedIn } = storeToRefs(store);
  watch(isLoggedIn, () => {
    if(!isLoggedIn.value) {
      router.push('/login');
    }
  }, { immediate: true });
}