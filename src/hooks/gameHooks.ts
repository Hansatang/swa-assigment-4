import { ref, watchEffect } from 'vue';
import type { Game } from '@/domain/game';
import env from '../environment';
import { useAuthStore } from '../store/authStore';
import { okOrReject } from '@/utils/utils';
import { useAuthenticatedFetch } from './authHooks';

export function useHighScores() {
  const authedFetch = useAuthenticatedFetch();
  const highScores = ref<Game[] | undefined>(undefined);

  watchEffect(() => {
    authedFetch(`${env.apiBaseUrl}/games`)
      .then<Game[]>((r) => (r.ok ? r.json() : Promise.reject()))
      .then((gs) =>
        gs
          .filter((g) => g.completed)
          .sort((g1, g2) => g2.score - g1.score)
          .slice(0, 10)
      )
      .then((gs) => (highScores.value = gs));
  });

  return highScores;
}

export function useGame() {
  const authedFetch = useAuthenticatedFetch();
  const store = useAuthStore();
  const user = store.currentUser;
  const game = ref<Game | null>(null);
  const error = ref<string | null>(null);

  const updateGame: (updated: Game) => Promise<void> = (updated: Game) => {
    if (!game.value) return Promise.reject('Game not started');

    return authedFetch(`${env.apiBaseUrl}/games/${game.value.id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(updated),
    })
      .then(okOrReject)
      .then(() => (game.value = updated))
      .catch((err) => (error.value = err));
  };

  const updateScore = (score: number) => {
    return game.value
      ? updateGame({ ...game.value, score })
      : Promise.reject('Cannot complete game');
  };

  const completeGame = () => {
    return game.value
      ? updateGame({ ...game.value, completed: true })
      : Promise.reject('Cannot complete game');
  };

  watchEffect(() => {
    if (!user || game.value) return;

    authedFetch(`${env.apiBaseUrl}/games`, {
      method: 'POST',
      body: JSON.stringify({ user }),
    })
      .then(okOrReject)
      .then((r) => r.json())
      .then((g) => (game.value = g))
      .catch((err) => (error.value = err));
  });

  return { game, error, updateScore, completeGame };
}
