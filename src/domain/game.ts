export type Game = {
	id: string,
	user: string,
	score: number,
	completed: boolean
};
