import {computed, ref} from 'vue'
import {defineStore} from 'pinia'
import type { User } from "../domain/user"

export const useAuthStore = defineStore('user', () =>{
    const currentUser = ref<User | null>(null);
    const currentToken = ref<string | null>(null);

    const isLoggedIn = computed(() => !!currentToken.value)

    function login(user: User, token: string) {
      currentUser.value = user;
      currentToken.value = token;
    };

    function logout() {
        currentUser.value = null;
        currentToken.value = null;  
    };

    function updateProfile(user: User) {
        if (!currentUser.value) {
            return;
        }
        currentUser.value = user;
    };

    return { currentUser, currentToken, isLoggedIn, login, logout, updateProfile };
});