export type Generator<T> = { next: () => T };

export type Position = {
  row: number;
  col: number;
};

export type Match<T> = {
  matched: T;
  positions: Position[];
};

export type Board<T> = {
  width: number;
  height: number;
  gen: Generator<T>;
  board: (T | undefined)[][];
};

export type Effect<T> = {
  kind: 'Match' | 'Refill';
  match?: Match<T>;
  board?: Board<T>;
};

export type MoveResult<T> = {
  board: Board<T>;
  effects: Effect<T>[];
};

export function createBoard<T>(generator: Generator<T>, width: number, height: number): Board<T> {
  return {
    gen: generator,
    width: width,
    height: height,
    board: buildBoard(width, height, generator),
  };
}

export function positions<T>(board: Board<T>): Position[] {
  const positions = [];
  for (let i = 0; i < board.height; i++) {
    for (let j = 0; j < board.width; j++) {
      positions.push({ row: i, col: j });
    }
  }
  return positions;
}

function buildBoard<T>(width: number, height: number, gen: Generator<T>): T[][] {
  const newBoard = [];
  for (let i = 0; i < height; i++) {
    const row = [...Array(width)].map((i) => gen.next());
    newBoard.push(row);
  }

  return newBoard;
}

export function piece<T>(board: Board<T>, p: Position): T | undefined {
  return isOnBoard(board, p) ? board.board[p.row][p.col] : undefined;
}

function isOnBoard<T>(board: Board<T>, p: Position): boolean {
  return p.row >= 0 && p.row < board.height && p.col >= 0 && p.col < board.width;
}

export function canMove<T>(board: Board<T>, first: Position, second: Position): boolean {
  if (first.row !== second.row && first.col !== second.col) return false;

  if (!isOnBoard(board, first) || !isOnBoard(board, second)) return false;

  const matches = findMatches(swap(board, first, second));

  return matches.length > 0;
}

function swap<T>(board: Board<T>, p1: Position, p2: Position): Board<T> {
  const newBoard = copyBoard(board);

  const swp = piece(newBoard, p1)!;
  newBoard.board[p1.row][p1.col] = newBoard.board[p2.row][p2.col];
  newBoard.board[p2.row][p2.col] = swp;

  return newBoard;
}

function findMatches<T>(board: Board<T>): Match<T>[] {
  const matches = findVerticalMatches(board);

  const m = findHorizontalMatches(board);
  if (m) matches.push(...m);

  return matches;
}

function findHorizontalMatches<T>(board: Board<T>) {
  const matches = [];

  for (let i = 0; i < board.height; i++) {
    matches.push(
      ...findSegments(board, { row: i, col: 0 }, (p) => ({
        row: p.row,
        col: p.col + 1,
      }))
    );
  }

  return matches;
}

function findVerticalMatches<T>(board: Board<T>) {
  const matches = [];

  for (let i = 0; i < board.width; i++) {
    matches.push(
      ...findSegments(board, { row: 0, col: i }, (p) => ({
        row: p.row + 1,
        col: p.col,
      }))
    );
  }

  return matches;
}

function findSegments<T>(
  board: Board<T>,
  start: Position,
  next: (p: Position) => Position
): Match<T>[] {
  const matches: Match<T>[] = [];

  let prevPos = start;
  let currentPos = next(start);
  let cnt = 1;
  let positions = [prevPos];
  while (isOnBoard(board, currentPos)) {
    if (piece(board, prevPos) === piece(board, currentPos)) {
      cnt++;
      positions.push(currentPos);
    } else {
      if (cnt >= 3) {
        matches.push({
          matched: piece(board, prevPos)!,
          positions: positions,
        });
      }

      cnt = 1;
      positions = [currentPos];
    }

    prevPos = currentPos;
    currentPos = next(currentPos);
  }

  if (cnt >= 3) {
    matches.push({
      matched: piece(board, prevPos)!,
      positions: positions,
    });
  }

  return matches;
}

export function move<T>(
  generator: Generator<T> | null,
  board: Board<T>,
  first: Position,
  second: Position
): MoveResult<T> {
  if (!canMove(board, first, second)) {
    return {
      board: board,
      effects: [],
    };
  }

  const newBoard = swap(board, first, second);

  const effects = [];

  let proccessed = undefined;
  let refilled = undefined;

  let matches = findMatches(newBoard);

  while (matches.length > 0) {
    if (refilled) {
      proccessed = processMatches(refilled.board, matches);
    } else {
      proccessed = processMatches(newBoard, matches);
    }

    refilled = refill(proccessed.board);
    effects.push(...proccessed.effects, ...refilled.effects);
    matches = findMatches(refilled.board);
  }

  return { board: refilled?.board ?? board, effects };
}

function processMatches<T>(board: Board<T>, matches: Match<T>[]): MoveResult<T> {
  const copy = copyBoard(board);
  const effects: Effect<T>[] = [];

  matches.forEach((m) => {
    m.positions.forEach((p) => {
      copy.board[p.row][p.col] = undefined;
    });

    effects.push({
      kind: 'Match',
      match: m,
    });
  });

  return {
    board: copy,
    effects: effects,
  };
}

function refill<T>(board: Board<T>): MoveResult<T> {
  const copy = copyBoard(board);
  const effects: Effect<T>[] = [];

  for (let i = 0; i < copy.width; i++) {
    refillColumn(copy, i);
  }

  effects.push({
    kind: 'Refill',
    board: copy,
  });

  return {
    board: copy,
    effects: effects,
  };
}

function refillColumn<T>(board: Board<T>, col: number): void {
  let offset = 0;
  for (let i = board.height - 1; i >= 0; i--) {
    const piece = board.board[i][col];

    if (!piece) offset++;
    else board.board[i + offset][col] = board.board[i][col];
  }

  for (let i = offset - 1; i >= 0; i--) board.board[i][col] = board.gen.next();
}

function copyBoard<T>(board: Board<T>): Board<T> {
  return {
    ...board,
    board: board.board.map((row) => [...row]),
  };
}
